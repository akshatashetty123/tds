package com.itt.tds.cfg;

import java.io.IOException;
import java.util.Properties;

public class TDSConfiguration {
	private static TDSConfiguration tdsConfig;

	private TDSConfiguration() {
	}

	public static TDSConfiguration getInstance() {
		if (tdsConfig == null) {
			tdsConfig = new TDSConfiguration();
		}
		return tdsConfig;
	}

	public String getDBConnectionString() throws IOException {
		String url = "jdbc:mysql://localhost:3306/TDS";
		String username = "root";
		String password = "ITT@123456";
		Properties properties = new Properties();
		properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
		url = properties.getProperty("jdbc.url");
		username = properties.getProperty("jdbc.username");
		password = properties.getProperty("jdbc.password");
		url = url + "?user=root&password=ITT@123456&useSSL=false";
		System.out.println("Connection created" + url);
		return url;
	}

	public static void main(String[] args) throws IOException {
		getInstance().getDBConnectionString();
	}
}