package com.itt.tds.comm;

import java.util.Hashtable;

public class TDSProtocol {
	private String sourceIp;
	private int sourcePort;
	private String destIp;
	private int destPort;
	private String protocolVersion;
	private String protocolFormat;
	protected String protocolType;
	private Hashtable<String, String> headers;
	private byte[] data;

	public TDSProtocol() {
		headers = new Hashtable<String, String>();
	}

	public String getProtocolVersion() {
		return protocolVersion;
	}

	public void setProtocolVersion(String protocolVersion) {
		this.protocolVersion = protocolVersion;
	}

	public String getSourceIp() {
		return sourceIp;
	}

	public void setSourceIp(String sourceIp) {
		this.sourceIp = sourceIp;
	}

	public String getDestIp() {
		return destIp;
	}

	public void setDestIp(String destIp) {
		this.destIp = destIp;
	}

	public int getSourcePort() {
		return sourcePort;
	}

	public void setSourcePort(int sourcePort) {
		this.sourcePort = sourcePort;
	}

	public int getDestPort() {
		return destPort;
	}

	public void setDestPort(int destPort) {
		this.destPort = destPort;
	}

	public String getProtocolFormat() {
		return protocolFormat;
	}

	public void setProtocolFormat(String protocolFormat) {
		this.protocolFormat = protocolFormat;
	}

	public String getProtocolType() {
		return protocolType;
	}

	public Hashtable<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(Hashtable<String, String> headers) {
		this.headers = headers;
	}

	public void setHeader(String key, String value) {
		this.headers.put(key, value);
	}
	public String getHeader(String key) {
		return this.headers.get(key);
	}
	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}
}