package com.itt.tds.comm;

import java.util.Hashtable;

public class TDSResponse extends TDSProtocol {
	Hashtable<String, String> headers = new Hashtable<String, String>();
	String Status = "status";

	public TDSResponse() {

	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		this.Status = status;
	}

	public String getValue(String key) {
		Hashtable<String, String> headers = getHeaders();
		return headers.get(key);
	}

	public void setValue(String key, String value) {
		headers.put(key, value);
		setHeaders(headers);
	}
}