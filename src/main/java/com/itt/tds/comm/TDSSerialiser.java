package com.itt.tds.comm;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface TDSSerializer
{
	public <T> T DeSerialize(String data, Class<T> exp) throws JsonParseException, JsonMappingException, IOException;
	public String Serialize(TDSProtocol protocol) throws JsonProcessingException;
} 
