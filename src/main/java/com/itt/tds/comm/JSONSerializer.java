package com.itt.tds.comm;

import java.io.IOException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;



public class JSONSerializer implements TDSSerializer{
	
	public <T> T DeSerialize(String data,Class <T> test) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		 T protocol = mapper.readValue(data,test);
		return  (T) protocol;
	}
	public String Serialize(TDSProtocol protocol) throws JsonProcessingException 
	{
		ObjectMapper objMapper = new ObjectMapper();
		String json = objMapper.writeValueAsString(protocol);
		return json;
	}
	
	public static void main(String [] args) throws JsonParseException, JsonMappingException, IOException 
	{
		String data = "{\"protocolVersion\":\"1.0\",\"protocolFormat\":\"JSON\",\"protocolType\":\"response\",\"sourceIp\":\"127.0.0.1\",\"sourcePort\":10001,\"destIp\":\"127.0.0.1\",\"destPort\":10002,\"headers\":{\"error-message\":\"message\",\"status\":\"success\",\"node-id\":\"121\",\"error-code\":\"404\"},\"data\":null}";
		TDSRequest reqObject = new TDSRequest();
		reqObject.setProtocolVersion("1.0");
		reqObject.setProtocolFormat("JSON");
		reqObject.setSourceIp("127.0.0.1");
		reqObject.setSourcePort(10001);
		reqObject.setDestIp("127.0.0.1");
		reqObject.setDestPort(10002);
		reqObject.setMethod("node-add");
		reqObject.setParameters("node-ip", "127.0.0.1");
		reqObject.setParameters("node-name", "node1");
		JSONSerializer ser = new JSONSerializer();
		String str= ser.Serialize(reqObject);
		
		System.out.println(str);
	}

} 