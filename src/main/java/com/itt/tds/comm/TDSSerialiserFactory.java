package com.itt.tds.comm;

public class TDSSerializerFactory {
	public static TDSSerializer serializer(String protocolFormat) throws Exception {
		TDSSerializer dataSerializer = getSerializer(protocolFormat);

		if(dataSerializer == null) {
			throw new Exception("Invalid TDSSerializser Format : " + protocolFormat);
		}
		return dataSerializer;
	}
	public static TDSSerializer getSerializer(String protocolFormat) {
		if(protocolFormat == "JSON") {
			return new JSONSerializer();
		}
		else {
			return null;
		}
	}
} 
