package com.itt.tds.comm;

import java.util.Hashtable;

public class TDSRequest extends TDSProtocol {
	Hashtable<String, String> headers = new Hashtable<String, String>();
	String method = "node_add";

	public TDSRequest() {

	}

	public void setValue(String parameter, String value) {
		headers.put(parameter, value);
		setHeaders(headers);
	}

	public String getValue(String key) {
		Hashtable<String, String> data = getHeaders();
		return data.get(key);
	}

	public void setMethod(String node_add) {
		this.method = node_add;
	}

	public String getMethod() {
		return method;
	}
}
