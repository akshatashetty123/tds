package com.itt.tds.core;

/**
 * 
 */
public class TaskResult {

	/**
	 * Default constructor
	 */
	public TaskResult() {
	}

	/**
	 * 
	 */
	public int errorCode;

	/**
	 * 
	 */
	public String errorMessage;

	/**
	 * 
	 */

	/**
	 * 
	 */
	public int taskId;

	/**
	 * 
	 */
	public TaskOutcome taskOutcome;

}