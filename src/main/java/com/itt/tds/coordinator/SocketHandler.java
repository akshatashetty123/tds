package com.itt.tds.coordinator;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import com.itt.tds.comm.TDSRequest;
import com.itt.tds.comm.TDSResponse;
import com.itt.tds.comm.TDSSerializer;
import com.itt.tds.comm.TDSSerializerFactory;

class SocketHandler implements Runnable{
	Socket sock;
	SocketHandler(Socket sock){
		this.sock = sock;
	}
	public void run(){
		try {
		String requestData = getRequest(sock);
		TDSSerializer tdsSerialiser = TDSSerializerFactory.serializer("JSON");
		TDSRequest request = tdsSerialiser.DeSerialize(requestData, TDSRequest.class);
		System.out.println(request);
		TDSController controller= RequestDispatcher.getController(request);
		TDSResponse response = TDSController.processRequest(request);
		TDSResponse responseSent = new TDSResponse();
		responseSent.setStatus("true");
		responseSent.setErrorCode("0");
		responseSent.setErrorMessage("Error Message"); 
		String responseData = tdsSerialiser.Serialize(responseSent);
				writeResponse(sock,responseData);
		}
		catch(Exception e){
			
		}
	}
	private String getRequest(Socket sock) throws IOException {
		DataInputStream dis = new DataInputStream(sock.getInputStream());	
		String  data = dis.readUTF();
		System.out.println(data);
		return data;
		
	}
	private void writeResponse(Socket sock,String responseData) throws IOException {
		DataOutputStream oos = new DataOutputStream(sock.getOutputStream());
		 oos.writeUTF(responseData);
	}
}