package com.itt.tds.coordinator;

import com.itt.tds.comm.TDSRequest;

class RequestDispatcher
{
	public static TDSController getController(TDSRequest request) throws Exception 
	{
		if (request.getMethod().startsWith("node-")) 
		{
			NodeController nodeController = new NodeController(); 
			return nodeController;
		} 
		else
		{
			throw new Exception("The getMethod provided is incorrect");
		}
	}
}
