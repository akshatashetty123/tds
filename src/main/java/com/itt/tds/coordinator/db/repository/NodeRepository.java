package com.itt.tds.coordinator.db.repository;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.itt.tds.coordinator.Node;

/**
 * 
 */
public interface NodeRepository {

	public int Add(Node node) throws SQLException, IOException;

	public int Modify(Node node) throws SQLException, IOException;

	public int Delete(int nodeId) throws SQLException, IOException;

	public List<Node> GetAvailableNodes() throws SQLException, IOException;

	public List<Node> GetAllNodes() throws SQLException, IOException;

}