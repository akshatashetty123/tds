package com.itt.tds.coordinator.db.repository;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.itt.tds.coordinator.Node;
import com.itt.tds.coordinator.db.DBManager;

public class NodeRepoImplementation implements NodeRepository {

	public int Modify(Node node) throws SQLException, IOException {
		String query = "update  node set nodeStatus =? where nodeId =?";
		DBManager dbManager = DBManager.getInstance();
		Connection connection = null;
		try {
			connection = dbManager.getConnection();
			PreparedStatement preparedstmt = connection.prepareStatement(query);
			preparedstmt.setString(1, node.getNodeStatus());
			preparedstmt.setInt(2, node.getNodeId());
			int result = preparedstmt.executeUpdate();
			return result;
		} finally {
			dbManager.closeConnection(connection);
		}
	}

	public int Add(Node node) throws SQLException, IOException {
		String query = "INSERT INTO node (nodeStatus,nodeIp,nodePort)VALUES(?,?,?)";
		DBManager dbManager = DBManager.getInstance();
		Connection connection = null;
		connection = dbManager.getConnection();
		PreparedStatement preparedstmt = connection.prepareStatement(query);
		preparedstmt.setString(1, node.getNodeStatus());
		preparedstmt.setInt(2, node.getNodePort());
		preparedstmt.setString(3, node.getNodeIp());
		int result = preparedstmt.executeUpdate();
		dbManager.closeConnection(connection);
		return result;
	}

	public List<Node> GetAvailableNodes() throws SQLException, IOException {
		List<Node> nodes = new ArrayList<Node>();
		String query = "SELECT * FROM node WHERE nodeStatus = 1";
		DBManager dbManager = DBManager.getInstance();
		Connection connection = null;
		connection = dbManager.getConnection();
		Statement statement = connection.createStatement();
		ResultSet res = statement.executeQuery(query);
		res = statement.executeQuery(query);

		try {
			while (res.next()) {
				Node node = new Node();
				node.setNodeId(res.getInt("nodeId"));
				node.setNodeIp(res.getString("nodeIp"));
				node.setNodeStatus(res.getString("nodeStatus"));
				nodes.add(node);
			}
			return nodes;
		} finally {
			dbManager.closeConnection(connection);
		}
	}

	public int Delete(int nodeId) throws SQLException, IOException {
		String query = "DELETE FROM Node WHERE nodeId = ?";
		DBManager dbManager = DBManager.getInstance();
		Connection connection = null;
		connection = dbManager.getConnection();
		try {
			PreparedStatement preparedstmt = connection.prepareStatement(query);
			preparedstmt.setInt(1, nodeId);
			int result = preparedstmt.executeUpdate();
			return result;
		} finally {
			dbManager.closeConnection(connection);
		}
	}

	public List<Node> GetAllNodes() throws SQLException, IOException {
		List<Node> nodes = new ArrayList<Node>();
		String query = "SELECT * FROM node";
		DBManager dbManager = DBManager.getInstance();
		Connection connection = null;
		connection = dbManager.getConnection();
		Statement statement = connection.createStatement();
		ResultSet res = statement.executeQuery(query);
		try {
			while (res.next()) {
				Node node = new Node();
				node.setNodeId(res.getInt("nodeId"));
				node.setNodeIp(res.getString("nodeIp"));
				// processingnode.setNodeStatus();
				nodes.add(node);
			}
			return nodes;
		} finally {
			dbManager.closeConnection(connection);
		}
	}

	public static void main(String args[]) throws SQLException, IOException {
		NodeRepoImplementation nodeRepoImplementation = new NodeRepoImplementation();
		Node node = new Node();
		node.setNodeIp("127.5");
		node.setNodePort(23);
		node.setNodeStatus("2");
		node.setNodeId(3);

		nodeRepoImplementation.GetAllNodes();
	}
}
