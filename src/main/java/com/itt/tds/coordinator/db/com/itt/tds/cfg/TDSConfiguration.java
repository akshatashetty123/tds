package com.itt.tds.coordinator.db.com.itt.tds.cfg;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class TDSConfiguration {
	private static TDSConfiguration tds = null;

	public static void main(String args[]) throws NullPointerException, IOException {
		TDSConfiguration.getInstance();
		tds.getDBConnectionString();
	}

	private TDSConfiguration() {

	}

	public static TDSConfiguration getInstance() {
		if (tds == null) {
			tds = new TDSConfiguration();
		}
		return tds;

	}

	public String getDBConnectionString() throws IOException {
		Properties props = new Properties();
		FileInputStream in = new FileInputStream("src\\main\\java\\com\\itt\\tds\\cfg\\config.properties");
		props.load(in);
		in.close();
		String url = props.getProperty("jdbc.url");
		String username = props.getProperty("jdbc.username");
		String password = props.getProperty("jdbc.password");
		String conUrl = url + "?user=" + username + "&password=" + password + "";
		System.out.println(conUrl);
		return (conUrl);

	}

}