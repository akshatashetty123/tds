package com.itt.tds.coordinator.db.repository;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.itt.tds.client.Client;
import com.itt.tds.coordinator.db.DBManager;

public class ClientRepoImplementation implements ClientRepository 
{
	public int Add(Client client) throws SQLException, IOException 
	{
		Connection conn = null;

		DBManager dbobj = DBManager.getInstance();

		conn = dbobj.getConnection();
		try 
		{
			String query = "INSERT INTO Client (userName,hostname)VALUES(?,?)";
			PreparedStatement pstmt = conn.prepareStatement(query);
			System.out.println(client.getUserName() + client.getHostName());
			pstmt.setString(1, client.getUserName());
			pstmt.setString(2, client.getHostName());
			int v = pstmt.executeUpdate();
			return v;
		} 
		finally 
		{
			dbobj.closeConnection(conn);
		}

	}

	public int Modify(Client client) throws SQLException, IOException
	{

		DBManager dbobj = DBManager.getInstance();
		Connection conn = null;
		conn = dbobj.getConnection();
		try 
		{
			String query = "UPDATE client SET userName =?, hostname=? where clientId =?";
			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setString(1, client.getUserName());
			pstmt.setString(2, client.getHostName());
			pstmt.setInt(3, client.getClientId());

			int v = pstmt.executeUpdate();
			return v;
		} 
		finally 
		{
			dbobj.closeConnection(conn);
		}

	}

	public int Delete(int clientId) throws SQLException, IOException 
	{

		DBManager dbobj = DBManager.getInstance();
		Connection conn = null;
		conn = dbobj.getConnection();
		
		try 
		{
			String query = "DELETE FROM Client WHERE clientId = ?";
			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, clientId);
			int v = pstmt.executeUpdate();

			return v;
		} 
		finally 
		{
			
			dbobj.closeConnection(conn);
		}
	}

	public List<Client> getClients() throws SQLException, IOException 
	{
		List<Client> clients = new ArrayList<Client>();

		DBManager dbobj = DBManager.getInstance();
		Connection conn = null;
		
			conn = dbobj.getConnection();
		
		try 
		{
			conn = dbobj.getConnection();
			String query = "SELECT * FROM client";
			Statement statement = conn.createStatement();
			ResultSet rs = statement.executeQuery(query);
			while (rs.next())
			{
				Client client = new Client();
				client.setHostName(rs.getString("hostname"));
				client.setUserName(rs.getString("username"));
				clients.add(client);
			}
			return clients;
		} 
		finally
		{
			dbobj.closeConnection(conn);
		}
	}
}
