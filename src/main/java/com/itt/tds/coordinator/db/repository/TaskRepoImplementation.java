package com.itt.tds.coordinator.db.repository;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.itt.tds.coordinator.Node;
import com.itt.tds.coordinator.db.DBManager;
import com.itt.tds.core.Task;

public class TaskRepoImplementation implements TaskRepository {
	public int Add(Task taskInstance) throws SQLException, IOException {
		Connection connection = null;
		DBManager dbManager = DBManager.getInstance();
		connection = dbManager.getConnection();

		try {
			String query = "INSERT INTO Task(taskName, taskExePath, taskParameters, taskState, userID , assignedNodeId )VALUES(?,?,?,?,?,?)";
			PreparedStatement prepareStatement = connection.prepareStatement(query);
			prepareStatement.setString(1, taskInstance.getTaskName());
			prepareStatement.setString(2, taskInstance.getTaskExePath());
			prepareStatement.setString(3, taskInstance.getTaskParameters());
			prepareStatement.setString(4, taskInstance.getTaskState());
			prepareStatement.setInt(5, taskInstance.getUserId());
			prepareStatement.setInt(6, taskInstance.getAssingedNodeId());
			int result = prepareStatement.executeUpdate();
			return result;
		} finally {
			dbManager.closeConnection(connection);
		}
	}

	public void Delete(int taskId) throws SQLException, IOException {
		Connection connection = null;
		DBManager dbManager = DBManager.getInstance();
		connection = dbManager.getConnection();

		try {
			String query = "delete from Task where id = ?";
			PreparedStatement preparedStmt = connection.prepareStatement(query);
			preparedStmt.setInt(1, taskId);
			preparedStmt.execute();
		} finally {
			dbManager.closeConnection(connection);
		}
	}

	public int SetTaskStatus(int taskId, int status) throws SQLException, IOException {
		Connection connection = null;
		DBManager dbManager = DBManager.getInstance();
		connection = dbManager.getConnection();

		try {
			String query = "update Task SET taskStatus = " + status + " where taskId = " + taskId;
			PreparedStatement preparedStmt = connection.prepareStatement(query);
			return preparedStmt.executeUpdate();
		} finally {
			dbManager.closeConnection(connection);
		}
	}

	public int Modify(Task taskInstance) throws SQLException, IOException {
		Connection connection = null;
		DBManager dbManager = DBManager.getInstance();
		connection = dbManager.getConnection();
		try {
			String query = "update task SET taskState =  " + taskInstance.getTaskState() + " where taskId = "
					+ taskInstance.getId();
			System.out.println(query + taskInstance.getId());
			PreparedStatement preparedStmt = connection.prepareStatement(query);
			return preparedStmt.executeUpdate();
		} finally {
			dbManager.closeConnection(connection);
		}

	}

	public List<Task> GetTasksByClientId(int clientId) throws SQLException, IOException {
		Connection connection = null;
		DBManager dbManager = DBManager.getInstance();
		connection = dbManager.getConnection();

		List<Task> task = new ArrayList<Task>();
		String query = "select * from Task where userID = " + clientId + ";";
		Statement statement = connection.createStatement();
		ResultSet res = statement.executeQuery(query);
		while (res.next()) {
			Task t = new Task();
			t.setTaskName(res.getString("taskName"));
			t.setTaskExePath(res.getString("taskPath"));
			t.setUserId(res.getInt("userID"));
			t.setAssignedNodeId(res.getInt("assignedNodeId"));
			t.setId(res.getInt("taskId"));
			System.out.println(t);
			task.add(t);
		}
		return task;
	}

	public int AssignNode(Node node, int taskId) throws SQLException, IOException {
		Connection connection = null;
		DBManager dbManager = DBManager.getInstance();
		connection = dbManager.getConnection();
		try {
			String query = "update Task set assignedNodeId = " + node.getNodeId() + " where taskId =  " + taskId;
			System.out.println(query);
			PreparedStatement preparedStmt = connection.prepareStatement(query);
			return preparedStmt.executeUpdate();
		} finally {
			dbManager.closeConnection(connection);
		}
	}

	public List<Task> GetTasksByNodeId(int nodeId) throws SQLException, IOException {
		Connection connection = null;
		DBManager dbManager = DBManager.getInstance();
		connection = dbManager.getConnection();

		List<Task> task = new ArrayList<Task>();
		String query = "select * from Task where assignedNodeId = " + nodeId;
		Statement statement = connection.createStatement();
		ResultSet res = statement.executeQuery(query);
		try {
			while (res.next()) {
				Task t = new Task();
				t.setTaskName(res.getString("taskName"));
				t.setTaskExePath(res.getString("taskPath"));
				t.setUserId(res.getInt("userID"));
				t.setAssignedNodeId(res.getInt("assignedNodeId"));
				t.setId(res.getInt("taskId"));
				task.add(t);
			}
			return task;
		} finally {
			dbManager.closeConnection(connection);
		}
	}
}