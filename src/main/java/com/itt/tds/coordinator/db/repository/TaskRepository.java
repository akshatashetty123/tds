package com.itt.tds.coordinator.db.repository;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.itt.tds.coordinator.Node;

/**
 * 
 */
public interface TaskRepository {

	/**
	 * @param taskInstance
	 * @return
	 */
	// public int Add(Task taskInstance);

	/**
	 * @param taskId
	 * @throws SQLException
	 * @throws IOException
	 */
	public void Delete(int taskId) throws SQLException, IOException;

	/**
	 * @param taskInstance
	 */
	// public int Modify(Task taskInstance);

	/**
	 * @param taskId
	 * @param status
	 * @throws IOException
	 * @throws SQLException
	 */
	public int SetTaskStatus(int taskId, int status) throws SQLException, IOException;

	/**
	 * @param clientId
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public List<com.itt.tds.core.Task> GetTasksByClientId(int clientId) throws SQLException, IOException;

	/**
	 * @param taskId
	 * @return
	 */
	// public Task GetTaskById(int taskId);

	/**
	 * @param status
	 * @return
	 */

	/**
	 * @param nodeId
	 * @return
	 * @throws SQLException
	 * @throws IOException
	 */

	/**
	 * @param node
	 * @param taskId
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public int AssignNode(Node node, int taskId) throws SQLException, IOException;

}