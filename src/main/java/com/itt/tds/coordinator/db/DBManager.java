package com.itt.tds.coordinator.db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.itt.tds.cfg.TDSConfiguration;

public class DBManager {
	private static DBManager dbManager;

	private DBManager() {

	}

	public static DBManager getInstance() {
		if (dbManager == null) {
			dbManager = new DBManager();
		}
		return dbManager;
	}

	public Connection getConnection() throws SQLException, IOException {
		Connection connection = DriverManager.getConnection(TDSConfiguration.getInstance().getDBConnectionString());
		return connection;
	}

	public void closeConnection(Connection conn) throws SQLException {
		conn.close();

	}

}