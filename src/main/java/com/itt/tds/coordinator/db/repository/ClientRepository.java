package com.itt.tds.coordinator.db.repository;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

import com.itt.tds.client.Client;

/**
 * 
 */
public interface ClientRepository {

    /**
     * @param client 
     * @return
     * @throws IOException 
     * @throws SQLException 
     */
    public int Add(Client client) throws SQLException, IOException;

    /**
     * @param client
     * @return 
     * @throws IOException 
     * @throws SQLException 
     */
    public int Modify(Client client) throws SQLException, IOException;

    /**
     * @param clientId
     * @throws IOException 
     * @throws SQLException 
     */
    public int Delete(int clientId) throws SQLException, IOException;

    /**
     * @return
     * @throws IOException 
     * @throws SQLException 
     */
    public List<Client> getClients() throws SQLException, IOException;

}
