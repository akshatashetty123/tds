package com.itt.tds.coordinator;

public class Node {

	/**
	 * Default constructor
	 */
	public Node() {
	}

	/**
	 * 
	 */
	private String nodeIp;
	private int nodePort;
	private String nodeStatus;
	private int nodeId;

	/**
	 * @param taskInstance
	 */
	public void executeTask(Task taskInstance) {
		// TODO implement here
	}

	/**
	 * @param result
	 */
	public void postResults(TaskResult result) {
		// TODO implement here
	}

	/**
	 * 
	 */
	public void pingCoordinator() {
		// TODO implement here
	}

	/**
	 * @return
	 */
	public Task waitForTask() {
		// TODO implement here
		return null;
	}

	public void setNodeIp(String string) {
		this.nodeIp = string;
	}

	public void setNodePort(int a) {
		this.nodePort = a;
	}

	public void setNodeStatus(String string) {
		this.nodeStatus = string;
	}

	public String getNodeIp() {

		return nodeIp;
	}

	public void setNodeId(int i) {

		this.nodeId = i;
	}

	public int getNodePort() {
		// TODO Auto-generated method stub
		return nodePort;
	}

	public String getNodeStatus() {
		// TODO Auto-generated method stub
		return nodeStatus;
	}

	public int getNodeId() {
		// TODO Auto-generated method stub
		return nodeId;
	}

}
