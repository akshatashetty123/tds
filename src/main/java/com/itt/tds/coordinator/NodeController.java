package com.itt.tds.coordinator;

import java.io.IOException;
import java.sql.SQLException;
import com.itt.tds.comm.TDSRequest;
import com.itt.tds.comm.TDSResponse;
import com.itt.tds.coordinator.db.repository.NodeRepoImplementation;

public class NodeController implements TDSController
{
	private static final String ADD_REQ = "node-add";
	private static final String UPDATE_REQ = "node-modify";
	
    public NodeController() 
    {
    	
    }

    public void run()
    {
    	
    }

    public TDSResponse processRequest(TDSRequest request) throws SQLException, IOException
	{
		if(request.getMethod().equals(ADD_REQ))
		{
			return	AddNode(request);
		}
		else 
		{
			return ModifyNode(request);
		}
	}
	public TDSResponse AddNode(TDSRequest request) throws SQLException, IOException
	{
		NodeRepoImplementation nodeImplementation = new NodeRepoImplementation();
		Node node = new Node();
		node.setNodeIp(request.getParameters("node-ip"));
		node.setNodePort((Integer.parseInt(request.getParameters("node-port"))));
		node.setNodeStatus("1");
		nodeImplementation.Add(node);
		return null;
	}
	public TDSResponse ModifyNode(TDSRequest request) throws SQLException, IOException
	{
		NodeRepoImplementation nodeImplementation = new NodeRepoImplementation();
		Node node = new Node();
		int result = 0;
		node.setNodeStatus("2");
		node.setNodeId(2);
		nodeImplementation.Modify(node);
		return null;
	}
}