package com.itt.tds.coordinator;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class MultithreadedServer 
{ 
	private ServerSocket serverSocket;
	public MultithreadedServer(int Port) throws IOException 
	{
		serverSocket=new ServerSocket(Port);  
	}
	public void start() throws IOException
	{
		System.out.println("Connected");
		while(true)
		{
			Socket socket = serverSocket.accept();
			SocketHandler socketHandler = new SocketHandler(socket);
			socketHandler.run();
		}
	}
	
	public static void main(String[]args) throws IOException 
	{
		int port = 3330;
		MultithreadedServer server = new MultithreadedServer(port);
		server.start();
	}
} 

