import java.net.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.itt.tds.comm.JSONSerializer;
import com.itt.tds.comm.TDSRequest;
import com.itt.tds.comm.TDSResponse;
import java.io.*; 

public class ClientSocket 
{
	public static void main(String [] args) throws JsonProcessingException 
	{
		String serverName = "127.0.0.1";
		int port = 3330;
		try 
		{
			System.out.println("Server is connecting to " + serverName + " on port " + port);
			Socket client = new Socket(serverName, port);

			OutputStream oos = client.getOutputStream();

			DataOutputStream out = new DataOutputStream(oos);
			TDSRequest req=new TDSRequest();
			req.setMethod("node-add");
			req.setParameters("node-name","node1");
			req.setParameters("node-ip","127.1.0");
			req.setParameters("node-port","3346");
			JSONSerializer json = new JSONSerializer();
			String jsonString = json.Serialize(req);

			out.writeUTF(jsonString);
			InputStream is = client.getInputStream();
			DataInputStream dataStream = new DataInputStream(is);
			String response = dataStream.readUTF();
			TDSResponse res = json.DeSerialize(response, TDSResponse.class);
			System.out.println("Server is giving the status as " +res.getStatus());
			client.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
} 