package com.itt.tds.com;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.itt.tds.coordinator.Node;
import com.itt.tds.coordinator.db.repository.TaskRepoImplementation;
import com.itt.tds.core.Task;

public class TaskRepositoryImplementationTest {
	@Test
	public void testModifyTask() {
		// Arrange
		TaskRepoImplementation taskRepoObj = new TaskRepoImplementation();
		Task task = new Task();
		task.setTaskState(3);
		task.setId(3);
		// Act
		int result;
		try {
			result = taskRepoObj.Modify(task);
		}

		catch (Exception e) {
			result = 0;
		}
		// Assert
		assertEquals(0, result);

	}

	@Test
	public void negTestModifyTask() {
		// Arrange
		TaskRepoImplementation taskRepoObj = new TaskRepoImplementation();
		Task task = new Task();
		task.setTaskState(3);
		task.setId(2);
		// Act
		int result;
		try {
			result = taskRepoObj.Modify(task);
		} catch (Exception e) {
			result = 0;
		}
		// Assert
		assertEquals(0, result);

	}

	@Test
	public void testGetTasksByNodeId() throws SQLException, IOException {
		// Arrange

		int userID = 1;
		List<Task> task = new ArrayList<Task>();
		TaskRepoImplementation taskRepoObj = new TaskRepoImplementation();
		// Act
		task = taskRepoObj.GetTasksByClientId(userID);
		// Arrange
		assertEquals(false, task.size() > 0);
	}

	@Test
	public void testAssignNode() throws SQLException, IOException {
		// Arrange
		Node node = new Node();
		node.setNodeId(2);
		TaskRepoImplementation taskRepoObj = new TaskRepoImplementation();
		// Act
		int result = taskRepoObj.AssignNode(node, 3);
		System.out.println(result);
		// Assert
		assertEquals(0, result);
	}

	@Test
	public void negTestAssignNode() throws SQLException, IOException {
		// Arrange
		Node node = new Node();
		node.setNodeId(2);
		TaskRepoImplementation taskRepoObj = new TaskRepoImplementation();
		// Act
		int result = taskRepoObj.AssignNode(node, 1);
		System.out.println(result);
		// Assert
		assertEquals(1, result);
	}

	@Test
	public void TestSetTaskStatus() throws SQLException, IOException {
		// Arrange
		TaskRepoImplementation taskRepoObj = new TaskRepoImplementation();
		// Act
		int result = taskRepoObj.SetTaskStatus(0, 1);

		// Assert
		assertEquals(false, result > 0);
	}

	@Test
	public void testGetTasksByStatus() throws SQLException, IOException {
		// Arrange
		int status = 1;
		List<Task> task = new ArrayList<Task>();
		TaskRepoImplementation taskRepoObj = new TaskRepoImplementation();
		// Act
		task = taskRepoObj.GetTasksByNodeId(2);
		// Assert
		assertEquals(false, task.size() == 0);
	}

	@Test
	public void NegTestGetTasksByStatus() throws SQLException, IOException {
		// Arrange
		int status = 1;
		List<Task> task = new ArrayList<Task>();
		TaskRepoImplementation taskRepoObj = new TaskRepoImplementation();
		// Act
		task = taskRepoObj.GetTasksByNodeId(0);
		// Assert
		assertEquals(false, task.size() > 0);
	}
}
