package com.itt.tds.com;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.itt.tds.coordinator.Node;
import com.itt.tds.coordinator.db.repository.NodeRepoImplementation;

import junit.framework.TestCase;

public class NodeRepositoryTest extends TestCase {
	@Test
	public void testAdd() throws SQLException, IOException {
		// Arrange
		NodeRepoImplementation nodeImplementation = new NodeRepoImplementation();
		Node node = new Node();
		int result = 0;
		node.setNodeIp("192.168.125.65");
		node.setNodePort(2276);
		node.setNodeStatus("2");
		// Act
		result = nodeImplementation.Add(node);
		// Assert
		assertEquals(1, result);
	}

	@Test
	public void testModify() throws SQLException, IOException {
		// Arrange
		NodeRepoImplementation nodeImplementation = new NodeRepoImplementation();
		Node node = new Node();
		int result = 0;
		node.setNodeStatus("2");
		node.setNodeId(2);
		// Act
		result = nodeImplementation.Modify(node);
		// Assert
		assertEquals(1, result);
	}

	@Test
	public void negTestModify() {
		// Arrange
		NodeRepoImplementation nodeImplementation = new NodeRepoImplementation();
		Node node = new Node();
		int result = 0;
		node.setNodeStatus("1");
		node.setNodeId(4);
		try {
			// Act
			result = nodeImplementation.Modify(node);
		} catch (Exception e) {
			// TODO: handle exception
			result = 0;
		}

		// Assert
		assertEquals(1, result);
	}

	@Test
	public void testDelete() throws SQLException, IOException {
		// Arrange
		int result, id = 4;
		NodeRepoImplementation nodeImplementation = new NodeRepoImplementation();
		// Act
		result = nodeImplementation.Delete(id);
		// Assert
		assertEquals(0, result);
	}

	@Test
	public void testGetAailableNodes() throws SQLException, IOException {
		// Arrange
		NodeRepoImplementation nodeImplementation = new NodeRepoImplementation();
		List<Node> node = new ArrayList<Node>();
		// Act
		node = nodeImplementation.GetAvailableNodes();
		// Assert
		assertEquals(true, node.size() > 0);
	}
}