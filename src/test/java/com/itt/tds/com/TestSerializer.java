package com.itt.tds.com;

import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.Test;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.itt.tds.comm.TDSRequest;
import com.itt.tds.comm.TDSResponse;
import com.itt.tds.comm.TDSSerializer;
import com.itt.tds.comm.TDSSerializerFactory;

public class TestSerializer 
{
	static TDSRequest reqObject = new TDSRequest();
	static TDSResponse resObject = new TDSResponse();
	@BeforeClass
	public static void prepareResponseObject() 
	{
		resObject = new TDSResponse();
		resObject.setProtocolVersion("1.0");
		resObject.setProtocolFormat("JSON");
		resObject.setSourceIp("127.0.0.1");
		resObject.setSourcePort(10001);
		resObject.setDestIp("127.0.0.1");
		resObject.setDestPort(10002);
		resObject.setStatus("success");
		resObject.setErrorCode("404");
		resObject.setErrorMessage("message");
		resObject.setValue("node-id", "121");
	}

	@BeforeClass
	public static void prepareRequestObject()
	{
		reqObject = new TDSRequest();
		reqObject.setSourcePort(10001);
		reqObject.setDestIp("127.0.0.1");
		reqObject.setDestPort(10002);
		reqObject.setMethod("node-add");
		reqObject.setParameters("node-ip", "127.0.0.1");
		reqObject.setParameters("node-name", "node1");
	}


	@Test
	public void testDeSerializerWithRequest() throws Exception 
	{
		//Arrange
		String requestObjectJSONString = "{"
				+ "\"protocolVersion\":\"1.0\",\"protocolFormat\":\"JSON\",\"protocolType\":\"request\","
				+ "\"sourceIp\":\"127.0.0.1\",\"sourcePort\":221,\"destIp\":\"222\",\"destPort\":10002,\"headers\":"
				+ "{\"node-ip\":\"127.0.0.1\",\"node-name\":\"node1\",\"method\":\"node-add\""
				+ "},\"data\":null"
				+ "}";

		TDSSerializer dataSerializer = TDSSerializerFactory.getSerializer("JSON");
		//Act
		TDSRequest result = dataSerializer.DeSerialize(requestObjectJSONString, TDSRequest.class);

		//Asert
		assertEquals(result.getDestPort(), 10002);
		assertEquals(result.getProtocolFormat(), "JSON");
		assertEquals(result.getProtocolType(), "request");
		assertEquals(result.getMethod(), "node-add");
		assertEquals(result.getProtocolVersion(), "1.0");
	}
	

	@Test
	public void testDeSerializerWithResponse() throws Exception
	{
		//Arrange
		String requestObjectJSONString = "{"
				+ "\"protocolVersion\":\"1.0\",\"protocolFormat\":\"JSON\",\"protocolType\":\"response\",\"sourceIp\":\"127.0.0.1\","
				+ "\"sourcePort\":222,\"destIp\":\"127.0.0.1\",\"destPort\":1002,\"headers\":"
				+ "{"
				+ "\"error-message\":\"message\",\"status\":\"success\",\"node-id\":\"121\",\"error-code\":\"404\""
				+ "},\"data\":null}";

		
		//Act
		TDSSerializer dataSerializer = TDSSerializerFactory.getSerializer("JSON");
		TDSResponse result = dataSerializer.DeSerialize(requestObjectJSONString, TDSResponse.class);

		//Assert
		assertEquals(result.getDestPort(), 1002);
		assertEquals(result.getProtocolFormat(), "JSON");
		assertEquals(result.getProtocolType(), "response");
		assertEquals(result.getProtocolVersion(), "1.0");
		assertEquals(result.getErrorCode(), "404");
		assertEquals(result.getErrorMessage(), "message");
		assertEquals(result.getStatus(), "success");
	}
	
	@Test
	public void testTDSRequestSerialise() throws Exception
	{
		String jsonString = "{\"protocolVersion\":\"1.0\",\"protocolFormat\":\"json\",\"sourceIp\":\"192.168.5.57\",\"sourcePort\":8081,\"destIp\":\"192.168.0.0\",\"destPort\":8081,\"headers\":{\"node-ip\":\"192.168.5.57\",\"node-name\":\"node1\"}}";
		TDSSerializer tds = TDSSerializerFactory.getSerializer("json");
		String result = tds.Serialize(reqObject);
		
		assertEquals(result, jsonString);
	}
	
	@Test
	public void testSerializerWithResponse() throws JsonProcessingException 
	{
		 String responseObjectString = "{\"sourceIp\":\"127.0.0.1\",\"destIp\":\"127.0.0.1\",\"sourcePort\":10001,\"destPort\":10002,\"protocolFormat\":\"JSON\",\"protocolVersion\":\"1.0\",\"protocolType\":\"RESPONSE\",\"headers\":{\"node-id\":\"121\",\"error-message\":\"message\",\"error-code\":\"404\",\"status\":\"success\"},\"data\":null,\"status\":\"success\",\"errorMessage\":\"message\",\"errorCode\":\"404\"}";
		 TDSSerializer dataSerializer = TDSSerializerFactory.getSerializer("JSON");
		String result = dataSerializer.Serialize(resObject);
		assertEquals(result, responseObjectString);
	}
}