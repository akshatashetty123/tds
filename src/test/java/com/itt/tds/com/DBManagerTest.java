package com.itt.tds.com;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import com.itt.tds.coordinator.db.DBManager;

public class DBManagerTest {
	public void testDBConnection() throws SQLException, IOException {

		DBManager dbManager = DBManager.getInstance();

		Connection conn = dbManager.getConnection();

		assertNull(conn);

	}

	public void testDBCloseConnection() throws SQLException, IOException {
		DBManager dbManager = DBManager.getInstance();

		Connection conn = dbManager.getConnection();
		dbManager.closeConnection(conn);
		assertNotNull(conn);

	}

}
