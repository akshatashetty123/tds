package com.itt.tds.com;

import junit.framework.TestCase;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.itt.tds.client.Client;
import com.itt.tds.coordinator.db.repository.ClientRepoImplementation;

public class ClientTest extends TestCase
{

	public void testAddClient() throws SQLException, IOException 
		{
			//Arrange

			ClientRepoImplementation clientRepoObj = new ClientRepoImplementation();
			Client client = new Client();
			int result = 0;
			client.setUserName("varsha");
			client.setHostName("varsha.patil");

			//Act
			result = clientRepoObj.Add(client);
			//Assert		
			assertEquals(true, result>0);

		}

		public void testModifyClient() throws SQLException, IOException
		{
			//Arrange
			ClientRepoImplementation clientRepoObj = new ClientRepoImplementation();
			List<Client> clientList = new ArrayList<Client>();
			Client client = new Client();
			client.setHostName("ruhi.verma");
			client.setUserName("ruhi");
			client.setClientId(8);

			//Act
			int result = clientRepoObj.Modify(client);
			//Assert
			assertEquals(true, result==0);
		}
	
	public void testDeleteClients() throws Exception 
	{
		//Arrange

		ClientRepoImplementation clientRepoObj = new ClientRepoImplementation();
		int id = 6;
		//Act&&Assert
		assertEquals(true,clientRepoObj.Delete(id)==0);
	}

	public void testGetClients() throws Exception 
	{
		//Arrange

		ClientRepoImplementation clientRepoObj  = new ClientRepoImplementation();
		List<Client> clientList = new ArrayList<Client>();
		//Act
		clientList = clientRepoObj.getClients();
		//Assert

		assertEquals(true,clientList.size()>0);
	}	

}

